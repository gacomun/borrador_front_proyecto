import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../login-usuario/login-usuario.js'
import '../visor-usuario/visor-usuario.js'


/**
 * @customElement
 * @polymer
 */
class UserDashboard extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <login-usuario on-myevent="processEvent"></login-usuario>
      <span hidden$="{{!isLogged}}"><visor-usuario id="visorusuario"></visor-usuario></span>


    `;
  }
  static get properties() {
    return {
      isLogged: {
        type: Boolean,
        value: false
      }
    };
  }//end properties

  processEvent(e){
    console.log("Capturado evento de emisor");
    console.log(e);
    this.$.visorusuario.userid=e.detail.userId;
    this.isLogged=e.detail.isLogged;
  }

}//end class

window.customElements.define('user-dashboard', UserDashboard);
