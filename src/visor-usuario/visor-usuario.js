import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../visor-cuenta/visor-cuenta.js'
/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /* all: initial; */
          /* border: solid blue; */
        }
        .redbg{
          background-color:red;
        }
        .bluebg{
          background-color:blue;
        }
        .greenbg{
          background-color:green;
        }
        .greybg{
          background-color:grey;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <!-- <div class="row greybg">
        <div class="col-7 offset-1 redbg">Col 1</div>
        <div class="col-3  greenbg">Col 2</div>
        <div class="col-4  bluebg">Col 3</div>
        <button class="btn col-3 btn-dark">Login</button>
      </div> -->
      <h2>Soy [[first_name]] [[last_name]]</h2>
      <h2>mi email es [[email]]</h2>
      <button class="btn btn-info" on-click="verCuentas">Ver Cuentas</button>
      <span hidden$="{{show}}"><visor-cuenta id="visorcuenta"></visor-cuenta></span>
      <iron-ajax
        id="getUser"
        url="http://localhost:3000/apitechu/v2/users/{{userid}}"
        handle-as="json"
        on-response="showData"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      },
      last_name: {
        type: String
      },
      userid: {
        type: Number,
        observer: "_useridChanged" //convencion el "_"
      },
      show: {
        type: Boolean,
        value:true
      },
      email: {
        type: String
      }
    };
  }// End Properties

  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.first_name=data.detail.response.first_name;
    this.last_name=data.detail.response.last_name;
    this.email=data.detail.response.email;
  }

  showError(data){
    console.log("showError");
    console.log(data.detail.response);
  }

  _useridChanged(newValue,oldValue){
    console.log("Course value has changed");
    console.log("Old value was "+oldValue);
    console.log("New value is "+newValue);
    this.$.getUser.generateRequest();

  }

  verCuentas(e){
    console.log("El usuario ha pulsado el boton verCuentas");
    console.log(this.userid);
    this.show=false;
    this.$.visorcuenta.id_user=this.userid;

  }
}// End Class

window.customElements.define('visor-usuario', VisorUsuario);
