import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * @customElement
 * @polymer
 */
class FrontproyectoApp extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <h2>Soy [[first_name]] [[last_name]]</h2>
      <h2>mi email es [[email]]</h2>
      <h2>y tengo [[age]]</h2>
      <input type="text" value="{{first_name::input}}"/>
      <input type="range" min="18" value="{{age::input}}" max="99"/>
    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      },
      last_name: {
        type: String
      },
      age: {
        type: Number
      },
      email: {
        type: String
      }
    };
  }
}

window.customElements.define('frontproyecto-app', FrontproyectoApp);
