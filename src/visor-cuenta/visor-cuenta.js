import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-sort-column.js'
// import '@vaadin/vaadin-grid/vaadin-grid-filter-column.js'
/**
 * @customElement
 * @polymer
 */
class VisorCuenta extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /* all: initial; */
          border: solid blue;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>Detalle de la cuenta:</h2>
      <vaadin-grid items="[[accountItems]]">
        <vaadin-grid-sort-column header="#"><template>[[index]]</template></vaadin-grid-sort-column>
        <vaadin-grid-sort-column path="IBAN" header="IBAN"></vaadin-grid-sort-column>
        <vaadin-grid-column header="Saldo"><template>[[item.balance]]</template></vaadin-grid-column>
        <vaadin-grid-column path="currency" header="Divisa"></vaadin-grid-column>
      </vaadin-grid>
      <!-- <h2>Detalle de la cuenta:</h2> -->
      <!-- <dom-repeat items="[[accountItems]]">
          <template>
            <hr/>
            <h2>IBAN: {{item.IBAN}}</h2>
            <h2>saldo: {{item.balance}} {{item.currency}}</h2>
          </template>
      </dom-repeat> -->
      <iron-ajax
        id="listAccount"
        url="http://localhost:3000/apitechu/v2/accounts/?id_user={{id_user}}"
        handle-as="json"
        on-response="showData"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      id_user: {
        type: Number,
        observer: "_iduserChanged" //convencion el "_"
      },
      // IBAN: {
      //   type: String
      // },
      // currency: {
      //   type: String
      // },
      accountItems: {
        type: Array
      }
      // balance: {
      //   type: Number
      // }
    };
  }// End Properties

  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    // this.IBAN=data.detail.response[0].IBAN;
    // this.currency=data.detail.response[0].currency;
    // this.balance=data.detail.response[0].balance;
    this.accountItems=data.detail.response;
  }

  showError(data){
    console.log("showError");
    console.log(data.detail.response);
  }

  _iduserChanged(newValue,oldValue){
    console.log("Course value has changed");
    console.log("Old value was "+oldValue);
    console.log("New value is "+newValue);
    this.$.listAccount.generateRequest();

  }
}// End Class

window.customElements.define('visor-cuenta', VisorCuenta);
