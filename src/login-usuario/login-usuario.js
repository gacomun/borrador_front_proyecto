import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <input type="email" placeholder="email" value="{{email::input}}"/>
      <br/>
      <input type="password"  placeholder="password" value="{{password::input}}"/>
      <br/>
      <button on-click="login">Login</button>
      <span hidden$="{{!isLogged}}">Bienvenid@ de nuevo</span>
      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/apitechu/v2/login"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAjaxResponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      password: {
        type: String
      },
      isLogged: {
        type: Boolean,
        value: false
      },
      email: {
        type: String
      }
    };
  }//end properties

  login(e){
    console.log("El usuario ha pulsado el boton");

    var loginData={
      "email": this.email,
      "password": this.password
    }
    this.$.doLogin.body= JSON.stringify(loginData);
    this.$.doLogin.generateRequest();
    // console.log("login"+loginData); // No
    // console.log(loginData); //Ok
  }

  manageAjaxResponse(data){
    console.log("Llegaron los resultdos");
    console.log(data.detail.response);
    this.isLogged=true;
    this.dispatchEvent(
      new CustomEvent(
        "myevent", // si ponemos en mayusculas no funciona
        {
          "detail":{ //detail es obligatorio
            "userId" : data.detail.response.idUsuario,
            "isLogged" : this.isLogged
          }
        }
      )
    );
  }

  showError(error){
    console.log("Hubo un error");
    console.log(error);
    console.log(error.detail.request.xhr.response);
    // console.log(error.detail.request.xhr.response.mensaje);
    // console.log(error.detail.request.xhr.response.status);
  }

}//end class

window.customElements.define('login-usuario', LoginUsuario);
